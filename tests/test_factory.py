from mbquery import create_app


def test_config():
    assert not create_app().testing
    assert create_app({"TESTING": True}).testing


def test_server(client):
    response = client.get("/test_server")
    assert response.data == b"Todo esta funcionando correctamente :D"
