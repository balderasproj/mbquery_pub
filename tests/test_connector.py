import pytest


expected = [["Nombre", "App", "Apm"], ["Juan", "Perez", "Lopez"], ["Jonh","Scott", ""]]

@pytest.fixture
def connector():
    from mbquery.connector import Connector
    conn = Connector()
    return conn

@pytest.mark.skip(reason="Se debe probar el metodo _connection")
def test_connection(connector):
    assert expected == connector.connection(1)("NADA", 10)

def test_connect_td(connector):
    # Los datos pasados en la siguiente funcion son datos fake
    connector.connect_td("",10)
    assert expected == connector.get_result()
