import sqlite3
import pytest
from mbquery.db import get_db
from mbquery.repository.historyrepository import save_history

def test_save_history(app):
    with app.app_context():
        db = get_db()
        assert db is get_db()
        save_history("select x from y", 1, 10, "nada", 1, "tester_history", "ql", "b1")
        result = db.execute("Select * from history where status=10").fetchall()
        assert result[0]["query"] == "select x from y"

