import pytest
from mbquery import constant


@pytest.mark.skip(reason="Pruebas")
def test_add_user_command(runner, monkeypatch):
    class Recorder(object):
        called = False

    def fake_add_user():
        Recorder.called = True

    # monkeypatch.setattr("mbquery.auth_controller.add_user", fake_add_user)
    result = runner.invoke(args=["add-user",{"username":"user", "password":"passw"}])
    # result = runner.invoke(args=["add-user"])
    print("resultado:", result)
    print("resultado:", result.output)
    assert constant.CREATED_USER_OK in result.output
    assert Recorder.called
