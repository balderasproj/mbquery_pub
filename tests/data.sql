/* Usuarios */
INSERT INTO user (id, username, password, last_login) VALUES (1, 'miguel', 'pbkdf2:sha256:150000$PScbwoGz$5c418b02e381137daccbc753f4c47125351f0b649044904980e0cc941eb47842', NULL);
INSERT INTO user (id, username, password, last_login) VALUES (2, 'mbalderas', 'pbkdf2:sha256:150000$8805AKz0$690c337df433f6a5e8ef64c7d2e69a28cb5b125f48422c3841b8dd8225c03a2b', NULL);
INSERT INTO user (id, username, password, last_login) VALUES (3, 'jose', 'pbkdf2:sha256:150000$KS1ZTdPn$fce2c99864d7554d0d38e057389b8d66fe249e7bf6dabeeb4d298692143f489d', NULL);