from flask import (Blueprint, flash, g, redirect,
                   render_template, request, session, url_for, current_app)
from werkzeug.exceptions import abort
from mbquery.auth_controller import login_required
from mbquery.repository.history_repository import get_history, get_history_by_id, delete_history, update_history_des
from mbquery import constant


blueprint = Blueprint('history', __name__, url_prefix="/history")

@blueprint.route("/")
@login_required
def history():
    """Show history of queries"""
    data = {"history": get_history(), 
    "formats": constant.FORMATS}
    return render_template('history/history.html', data=data)

@blueprint.route("/del/<int:id>")
@login_required
def historydel(id):
    """Delete history"""
    delete_history(id)
    return ""

@blueprint.route("/upddes/<int:id>", methods=("GET", "POST"))
@login_required
def upddes(id):
    """Update description history"""
    print("Actualizando desde controller")
    if request.method == "GET":
        description = request.args.get("desc")
        update_history_des(id, description)
    return ""