INCORRECT_USER = "El usuario es incorrecto."
INCORRECT_PASSWORD = "El password es incorrecto."

USERNAME_MISSING = "No se indico el nombre de usuario."
PASSWORD_MISSING = "No se indico el password."
USERNAME_ALREADY_EXIST = "El usuario ya existe."

CREATED_USER_OK = "El usuario se ha creado de manera correcta."

STATUS_OK = 1
STATUS_FAIL = 0
STATUS_RUN = 3

SOURCE_TD = 1
SOURCE_IBM = 2
SOURCE_TD_UAT = 3
SOURCES = {"SOURCE_TD" : SOURCE_TD,
"SOURCE_IBM": SOURCE_IBM,
"SOURCE_TD_UAT": SOURCE_TD_UAT}

SOURCES_LIST = [[SOURCE_TD, "TERADATA"], [SOURCE_IBM, "IBM"], [SOURCE_TD_UAT, "UAT"]]

FORMAT_STD = "STD"
FORMAT_XLS = "XLS"
FORMAT_CSV = "CSV"
FORMAT_TXT = "TXT"
FORMAT_HTM = "HTM"
FORMATS = {"FORMAT_STD": FORMAT_STD,
"FORMAT_XLS": FORMAT_XLS,
"FORMAT_CSV": FORMAT_CSV,
"FORMAT_TXT": FORMAT_TXT,
"FORMAT_HTM": FORMAT_HTM}

EXTENTIONS = {FORMAT_XLS: ".xlsx",
FORMAT_CSV: ".csv",
FORMAT_TXT: ".txt",
FORMAT_HTM: ".html"}

LIMIT_1 = 1
LIMIT_1_VAL = 10
LIMIT_2 = 2
LIMIT_2_VAL = 20
LIMIT_3 = 3
LIMIT_3_VAL = 50
LIMIT_4 = 4
LIMIT_4_VAL = 100
LIMIT_5 = 5
LIMIT_5_VAL = 1000
LIMIT_6 = 6
LIMIT_6_VAL = 2000
LIMIT_7 = 7
LIMIT_7_VAL = 5000
LIMIT_8 = 8
LIMIT_8_VAL = 10000
LIMIT_9 = 9
LIMIT_9_VAL = 20000
LIMIT_10 = 10 
LIMIT_10_VAL = 50000
LIMIT_11 = 11 
LIMIT_11_VAL = 100000
LIMIT_12 = 12
LIMIT_12_VAL = "All"
LIMITS = {"LIMIT_1": LIMIT_1, "LIMIT_1_VAL": LIMIT_1_VAL,
"LIMIT_2": LIMIT_2, "LIMIT_2_VAL": LIMIT_2_VAL,
"LIMIT_3": LIMIT_3, "LIMIT_3_VAL": LIMIT_3_VAL,
"LIMIT_4": LIMIT_4, "LIMIT_4_VAL": LIMIT_4_VAL,
"LIMIT_5": LIMIT_5, "LIMIT_5_VAL": LIMIT_5_VAL,
"LIMIT_6": LIMIT_6, "LIMIT_6_VAL": LIMIT_6_VAL,
"LIMIT_7": LIMIT_7, "LIMIT_7_VAL": LIMIT_7_VAL,
"LIMIT_8": LIMIT_8, "LIMIT_8_VAL": LIMIT_8_VAL,
"LIMIT_9": LIMIT_9, "LIMIT_9_VAL": LIMIT_9_VAL,
"LIMIT_10": LIMIT_10, "LIMIT_10_VAL": LIMIT_10_VAL,
"LIMIT_11": LIMIT_11, "LIMIT_11_VAL": LIMIT_11_VAL,
"LIMIT_12": LIMIT_12, "LIMIT_12_VAL": LIMIT_12_VAL} 

LIMITS_LIST = [[LIMIT_1, LIMIT_1_VAL],
[LIMIT_2, LIMIT_2_VAL],
[LIMIT_3, LIMIT_3_VAL],
[LIMIT_4, LIMIT_4_VAL],
[LIMIT_5, LIMIT_5_VAL],
[LIMIT_6, LIMIT_6_VAL],
[LIMIT_7, LIMIT_7_VAL],
[LIMIT_8, LIMIT_8_VAL],
[LIMIT_9, LIMIT_9_VAL],
[LIMIT_10, LIMIT_10_VAL],
[LIMIT_11, LIMIT_11_VAL],
[LIMIT_12, LIMIT_12_VAL],
]

LIMITS_VALS = {
    LIMIT_1: LIMIT_1_VAL,
    LIMIT_2: LIMIT_2_VAL,
    LIMIT_3: LIMIT_3_VAL,
    LIMIT_4: LIMIT_4_VAL,
    LIMIT_5: LIMIT_5_VAL,
    LIMIT_6: LIMIT_6_VAL,
    LIMIT_7: LIMIT_7_VAL,
    LIMIT_8: LIMIT_8_VAL,
    LIMIT_9: LIMIT_9_VAL,
    LIMIT_10: LIMIT_10_VAL,
    LIMIT_11: LIMIT_11_VAL,
    LIMIT_12: LIMIT_12_VAL
}