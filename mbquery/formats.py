from openpyxl import Workbook
from openpyxl import load_workbook
import csv
from mbquery import constant
from mbquery.util import get_filename_result
from mbquery.repository.config_repository import get_config_delimiter
from flask import g


def _write_excel(file_name, sheet_name, data):
    """Write excel file with list data"""
    wb = Workbook()
    ws = wb.active
    ws.title = sheet_name
    _add_data_and_save_excel(wb, ws, file_name, data)
        

def _write_sheet(file_name, sheet_name, data):
    """Write sheet on excel file indicated"""
    wb = load_workbook(filename=file_name)
    ws = wb.create_sheet()
    ws.title = sheet_name
    _add_data_and_save_excel(wb, ws, file_name, data)


def _add_data_and_save_excel(wb, ws, file_name, data):
    """Add data to excel file and save on disk"""
    for row in data:
        ws.append(row)

    try:
        wb.save(file_name)
    except:
        print("Imposible write excel file:", sys.exc_info()[0])
        



def _render_template_html(list, query, sign, title="Data Results"):
    template = f"""
<!DOCTYPE html>
<html>

<head>
  <meta charset='UTF-8'>
  <title{title}</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
"""

    template += """
  <style>
  * { 
    margin: 0; 
    padding: 0; 
  }
  body { 
    font: 14px/1.4 Palatino, Serif; 
  }
  
  /* 
  Generic Styling, for Desktops/Laptops 
  */
  table { 
    width: 100%; 
    border-collapse: collapse; 
  }
  /* Zebra striping */
  tr:nth-of-type(odd) { 
    background: #eee; 
  }
  th { 
    background: #333; 
    color: white; 
    font-weight: bold; 
  }
  td, th { 
    padding: 6px; 
    border: 1px solid #9B9B9B; 
    text-align: left; 
  }
  @media 
  only screen and (max-width: 760px),
  (min-device-width: 768px) and (max-device-width: 1024px)  {
    table, thead, tbody, th, td, tr { display: block; }
    thead tr { position: absolute;top: -9999px;left: -9999px;}
    tr { border: 1px solid #9B9B9B; }
    td { border: none;border-bottom: 1px solid #9B9B9B; position: relative;padding-left: 50%; }
    
    td:before { position: absolute;top: 6px;left: 6px;width: 45%; padding-right: 10px; white-space: nowrap;}
    
    /*
    Label the data
    */
    
    """

    for columname in list[0]:
        template += 'td:nth-of-type(0):before { content: "' + columname + '"; }\n'

    template += """
  }

  /* Smartphones (portrait and landscape) ----------- */
  @media only screen
  and (min-device-width : 320px)
  and (max-device-width : 480px) {
    body {
      padding: 0;
      margin: 0;
      width: 320px; }
    }

  /* iPads (portrait and landscape) ----------- */
  @media only screen and (min-device-width: 768px) and (max-device-width: 1024px) {
    body {
      width: 495px;
    }
  }

  </style>
  <!--<![endif]-->
<script type="text/javascript">

function search(){

  var s = document.getElementById('search').value;

  rows = document.getElementById('data').getElementsByTagName('TR');
  for(var i=0;i<rows.length;i++){
    if ( rows[i].textContent.indexOf(s)>-1  || s.length==0 ) {
	  rows[i].style.display ='';
    } else {
      rows[i].style.display ='none';
    }
  }
}


var timer;
function delayedSearch() {
	clearTimeout(timer);
	console.log('delay-ing')
    timer = setTimeout(function () {
		console.log('delay-running')
		search();
    }, 500);
  }</script>
</head> 
"""

    template += f"""
<body>
	<code>
		SQL> {query}
	</code>
<div><input type="text" size="30" maxlength="1000" value="" id="search" onkeyup="delayedSearch();" /><input type="button" value="Go" onclick="search();"/> </div>
<table><thead><tr> """

    for columname in list[0]:
        template += f"<th>{columname}</th>"

    template += """
</tr></thead>
<tbody id="data"> 
	"""

    for row in list[1:]:
        template += "<tr>\n"

        for value in row:
            template += f"<td>{value}</td>"

        template += "</tr>\n"

    template += f"""
</tbody></table>
<p><span>&#171;</span>{sign}<span>&#187;</span></p>
</body></html>
    """

    return template


def _excel_format(file_name, data, query):
    """Write a excel file with data and query"""
    _write_excel(file_name, "Result", data)
    _write_sheet(file_name, "Query", [(query,)])
    return file_name


def _txt_format(file_name, data, delimiter):
    """Write a file with data and with the delimiter indicated"""    
    with open(file_name, mode="w", newline="", encoding="utf-8") as txt:
        writer = csv.writer(txt, delimiter=delimiter)
        for row in data:
            writer.writerow(row)
    return file_name


def _csv_format(file_name, data):
    return _txt_format(file_name, data, ",")


def _html_format(file_name, data, query, sign="MB-HTML", title="Data Results"):
    with open(file_name, "w") as html:
        html.writelines(_render_template_html(data, query, sign, title))
    return file_name
      

def formater(result, query, format=constant.FORMAT_STD):
    print("En formater")    
    filename, filename_path = get_filename_result()
    filename += constant.EXTENTIONS.get(format)
    filename_path += constant.EXTENTIONS.get(format)
    delimiter = get_config_delimiter(g.user["id"])
    print("filename:", filename)    

    if format==constant.FORMAT_XLS:
        return filename, _excel_format(filename_path, result, query)
    elif format==constant.FORMAT_TXT:
        return filename, _txt_format(filename_path, result, delimiter)
    elif format==constant.FORMAT_CSV:
        return filename, _csv_format(filename_path, result)
    elif format==constant.FORMAT_HTM:
        return filename, _html_format(filename_path, result, query)