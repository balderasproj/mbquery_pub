from flask import (Blueprint, flash, g, redirect,
                   render_template, request, session, url_for, current_app)
from werkzeug.exceptions import abort
from mbquery.auth_controller import login_required
from mbquery.db import get_db
from mbquery.database import Database
from mbquery.repository.history_repository import get_history_by_id
from mbquery.repository.config_repository import get_config_limit, get_config_source
from mbquery.connector import Connector
from mbquery import constant
from flask import current_app as app
from flask import send_file
import os
from mbquery.formats import formater
from mbquery.util import get_string_from_list


blueprint = Blueprint('sqlws', __name__)

@blueprint.route("/", methods=("GET", "POST"))
@login_required
def index():
    if request.method == "GET":
        query = request.args.get("q")

    if query == None:
        query = ""
    else:
        history = get_history_by_id(query)
        query = history[0]["query"]

    
    limit = get_config_limit(g.user["id"])
    source = get_config_source(g.user["id"])

    data = {"SOURCES": constant.SOURCES_LIST,
     "FORMATS": constant.FORMATS,
     "LIMITS": constant.LIMITS_LIST,
     "query": query,
     "limit": limit, 
     "source": source}
    return render_template("sqlws/index.html", data=data)

@blueprint.route("/result", methods=("GET", "POST"))
@login_required
def result():
    """Retorna el resultado de un query"""
    if request.method == "GET":
        source = request.args.get("source")
        query = request.args.get("query")
        limit = request.args.get("limit")
        format = request.args.get("format")

        conn = Connector()
        conn.exec_query(source, query, limit)
        if format==constant.FORMAT_STD:
            return get_string_from_list(conn.get_result(), conn.get_duration(), conn.get_history_id())
        else:
            filename, filename_path = formater(conn.get_result(), query, format)
            return format_result(filename, filename_path, conn.get_duration())

def format_result(filename, filename_path, duration=0):
    url_file = url_for("sqlws.download", filename=filename)
    url_img = url_for("static", filename=f"img/{filename[filename.find('.')+1:]}.png")
    file_size = os.path.getsize(filename_path)
    result = {"file_name": url_file, "img_name": url_img, "file_size": file_size, "duration": duration}
    return result


@blueprint.route("/sample")
def sample():
    file = os.path.join(app.root_path, app.config["RESULT_FILES"], "prueba.txt")
    return send_file(file, as_attachment=True)

@blueprint.route("/download/<filename>")
@login_required
def download(filename):
    file = os.path.join(app.root_path, app.config["RESULT_FILES"], filename)
    return send_file(file, as_attachment=True)

@blueprint.route("/export/<int:id>/<format>")
@login_required
def export(id, format):
    # Generar archivo con los datos del history indicado y en elformato indicado
    history = get_history_by_id(id)
    query = history[0]["query"]
    query_local = history[0]["query_local"]
    base_local = history[0]["base_local"]
    print("query local:", query_local)

    base = Database(os.path.join(app.instance_path, base_local))
    result = base.execute_and_fechtall(query_local)
    filename, filename_path = formater(result, query, format)

    return redirect(url_for("sqlws.download",filename=filename))