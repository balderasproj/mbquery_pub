from flask import url_for

def get_html_pane_dropdown(id, formats):
    return f"""<div class="btn-group">
            <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-cloud-download-fill" viewBox="0 0 16 16">
                    <path fill-rule="evenodd" d="M8 0a5.53 5.53 0 0 0-3.594 1.342c-.766.66-1.321 1.52-1.464 2.383C1.266 4.095 0 5.555 0 7.318 0 9.366 1.708 11 3.781 11H7.5V5.5a.5.5 0 0 1 1 0V11h4.188C14.502 11 16 9.57 16 7.773c0-1.636-1.242-2.969-2.834-3.194C12.923 1.999 10.69 0 8 0zm-.354 15.854a.5.5 0 0 0 .708 0l3-3a.5.5 0 0 0-.708-.708L8.5 14.293V11h-1v3.293l-2.146-2.147a.5.5 0 0 0-.708.708l3 3z"/>
                </svg>
            </button>
            <div class="dropdown-menu">
                <a class="dropdown-item" href="{ url_for('sqlws.export', id=id, format=formats['FORMAT_XLS'])}">Excel</a>
                <a class="dropdown-item" href="{ url_for('sqlws.export', id=id, format=formats['FORMAT_CSV'])}">csv</a>
                <a class="dropdown-item" href="{ url_for('sqlws.export', id=id, format=formats['FORMAT_TXT'])}">txt</a>
                <a class="dropdown-item" href="{ url_for('sqlws.export', id=id, format=formats['FORMAT_HTM'])}">html</a>
            </div>
            </div>
"""

def get_html_config_source(source, sources):
    template = f"""
              <div class="btn-group btn-group-toggle" data-toggle="buttons"> 
            """
    for src in sources:
        template += f"""
                <label class="btn btn-secondary {'active' if src[0] == source else ''}">
                  <input type="radio" name="config_opt_source" id="opt-{src[0]}" value="{src[0]}" {'checked' if src[0] == source else ''}>{src[1]}</label>"""
    template += """
              </div>
    """

    return template

def get_html_config_limit(limit, limits):
    print("limit:", limit)
    template = """<select class="form-control" id="config_output-limit">"""
    for lim in limits:
        template += f"""<option value="{lim[0]}" {'selected="Selected"' if lim[0] == limit else ''}>{lim[1]}</option>"""
    template += "</select>"

    return template