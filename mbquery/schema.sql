drop table if exists history;
drop table if exists tables;
drop table if exists source;
drop table if exists config;
drop table if exists status;
drop table if exists user;

/* Almacena el historial de consultas realizadas desde ell sql workshop */
create table history(
    id integer Primary Key autoincrement,
    exec_date datetime default current_timestamp,
    query TEXT, 
    source integer,
    status integer,
    project Varchar(50),
    duration numeric,
    user_id numeric,
    query_local TEXT,
    base_local TEXT,
    description TEXT
);


/* Almacena los nombres de las tablas frecuentes, esto para usarlas a manera de shortcuts */
create table tables(
    id integer Primary Key autoincrement,
    table_name text,
    short_name varchar(10)
);

/* Almacena los origenes de datos */
create table source(
    id integer Primary Key, 
    name varchar(10)
);

--Defaults values, this values are the same like constants
Insert Into source (id, name) values (1, 'TD');
Insert Into source (id, name) values (2, 'IBM');
Insert Into source (id, name) values (3, 'TD UAT');

/* Almacena la configuracion del sistema */
create table config(
    id integer Primary Key autoincrement,
    project varchar(50),
    limit_rows integer,
    source integer,
    hist_dur_days integer,
    delimiter_txt varchar(1),
    vim_mode integer,
    dark_mode integer,
    user_id integer
);

/* Almacena los status usados en la app */
create table status(
    id numeric Primary Key,
    name varchar(15)
);

--Defaults values
Insert Into status (id, name) values (1, 'OK');
Insert Into status (id, name) values (0, 'Fail');
Insert Into status (id, name) values (3, 'Running');

/* Almacena los usuarios */
create table user(
    id integer Primary Key autoincrement,
    username varchar(15),
    password text,
    last_login datetime
);