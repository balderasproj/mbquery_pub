var running = false;
$(document).ready(function(){
    $("#btn-run").click(function(){
      exec()
    })
    $("#txt-sql").keydown(function(e){
      if ((e.ctrlKey && e.keyCode == 13) || e.keyCode == 120){
        exec()
      }
    });
    $("#txt-sql").focus();

});

function exec(){
  console.log("running:" + running)
  if (running){
    return
  }
  running = true
  source = $("input[type=radio][name=opt_source]:checked").val();
  limit = $("#output-limit").val();
  format = $("#output-format").val();
  query = $("#txt-sql").val();
  //alert("source:" + source)
  //alert("limit:" + limit)
  //alert("format:" + format)
  //alert("sql:" + query)
  if (query === ""){
    alert("Query is missing")
    $("#txt-sql").focus()
  }else{
    get_results(query, limit, source, format)
  }

}

function get_results(sql, limit, source, format){
    $.ajax({
        url: "/result",
        data: {query: sql, limit: limit, source: source, format: format},
        type: "GET",
        dataType: "json",
        beforeSend: function(status){
          show_progress_bar()
          $("#dropdown-export").html("")
        },
        success: function(json){
            show_preparing_bar()
            setTimeout(function(){
              if (format === "STD"){
                print_results(json)
              }
              else {
                print_download_file(json)
              }
              running = false
            }, 10)
        },
        ajaxSuccess: function(){
          console.log('en ajaxSuccess ')
        },
        error: function(xhr, status){
            console.log(status);
            show_error_bar()
        },
        complete: function(xhr, status){
            console.log("Completado" + status);
        }
});
}

function show_progress_bar(){
  console.log("En progres bar")
  $("#results").html(
    '<div class="row"> \
    <div class="col-12" align="center" > \
      <br> \
      <img src="/static/img/progress.gif"> \
      <p>Getting results from the server</p> \
    </div> \
  </div>')
}

function show_preparing_bar(){
  console.log("En preparing")
  $("#results").html(
    '<div class="row"> \
    <div class="col-12" align="center" > \
      <br> \
      <img src="/static/img/preparing.gif"> \
      <p>Preparing result...</p> \
    </div> \
  </div>')
}

function show_error_bar(){
  console.log("En error bar")
  $("#results").html(
    '<div class="row"> \
    <div class="col-12" align="center" > \
      <br> \
    <h2 class="text-danger">Ooops...</h2> \
    <h4 class="text-secondary">something went wrong</h4> \
      <img src="/static/img/error.gif"> \
    </div> \
  </div>')
}
//TODO: Quitar esta funcion
function proccess(json){
    console.log("In process");
    for (var key in json.result){
        for (var column in json.result[key]){
            console.log("row:" + key + " Column:" + column + " col val:" + json.result[key][column])
        }
    }
}

function print_download_file(json){
  console.log("En print download File")
  $("#results").html(
    '<div class="row"> \
    <div class="col-12" align="center" > \
      <br> \
      <h4>File with results</h4> \
      <a href="' + json.file_name + '"><img src="' + json.img_name + '" witdh="256" height="256"></a> \
      <p>Size: ' + json.file_size + ' bytes </p> \
    </div> \
  </div>')

}

function print_results(json){
    console.log("En print results");
    columns = '<tr>'
    for (var column in json.result[0]){
        columns += '<th scope="col">' + column + '</th>'
    }
    columns += '</tr>'

    rows = ''
    total = 0
    for (var key in json.result){
        rows += '<tr>'
        total += 1
        for (var column in json.result[key]){
            //console.log("row:" + key + " Column:" + column + " col val:" + results.result[key][column])
            rows += '<td>'
            rows += json.result[key][column]
            rows += '</td>'

        }
        rows += '</tr>'
    }

    $("#results").html(
    '<div class="row"> \
    <div class="col-6"> \
      <input id="search" class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search"> \
    </div> \
    <div class="col-6"> \
        <button id="btn-clear" type="button" class="btn btn-primary"> \
        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash2-fill" viewBox="0 0 16 16"> \
            <path d="M2.037 3.225A.703.703 0 0 1 2 3c0-1.105 2.686-2 6-2s6 .895 6 2a.702.702 0 0 1-.037.225l-1.684 10.104A2 2 0 0 1 10.305 15H5.694a2 2 0 0 1-1.973-1.671L2.037 3.225zm9.89-.69C10.966 2.214 9.578 2 8 2c-1.58 0-2.968.215-3.926.534-.477.16-.795.327-.975.466.18.14.498.307.975.466C5.032 3.786 6.42 4 8 4s2.967-.215 3.926-.534c.477-.16.795-.327.975-.466-.18-.14-.498-.307-.975-.466z"/> \
        </svg> & \
        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-unlock" viewBox="0 0 16 16"> \
          <path d="M11 1a2 2 0 0 0-2 2v4a2 2 0 0 1 2 2v5a2 2 0 0 1-2 2H3a2 2 0 0 1-2-2V9a2 2 0 0 1 2-2h5V3a3 3 0 0 1 6 0v4a.5.5 0 0 1-1 0V3a2 2 0 0 0-2-2zM3 8a1 1 0 0 0-1 1v5a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V9a1 1 0 0 0-1-1H3z"/> \
        </svg> \
        </button> \
    </div> \
  </div> \
<div class="row"> \
    <div class="col-12"> \
    <strong id="rows-total">' + total + ' rows retrieved on ' + json.duration + ' seconds</strong> \
    </div> \
 </div> \
  <div class="row"> \
    <div class="col-12"> \
      <table class="table"> \
        <thead> \
        ' + columns +
        ' </thead> \
        <tbody id="result-data"> \
        ' + rows +    
        '</tbody> \
      </table> \
    </div>');

    $("#dropdown-export").html(json.dropdown_export)

    $("#search").keyup(function(){
        delayedSearch()
    });

    $("#btn-clear").click(function(){
      $("#results").html("")
    })
}

function search(){

  var s = document.getElementById('search').value;

  rows = document.getElementById('result-data').getElementsByTagName('tr');
  total = 0
  for(var i=0;i<rows.length;i++){
    if ( rows[i].textContent.indexOf(s)>-1  || s.length==0 ) {
      rows[i].style.display ='';
      total += 1
    } else {
      rows[i].style.display ='none';
    }
  }
  $("#rows-total").html( + total + " rows")
}


var timer;
function delayedSearch() {
	clearTimeout(timer);
    timer = setTimeout(function () {
		search();
    }, 500);
  }