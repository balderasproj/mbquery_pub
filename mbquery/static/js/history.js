

$(document).ready(function(){

    $("#search").keyup(function(){
        delayedSearch()
    });
    
});

function search(){

  var s = document.getElementById('search').value;

  rows = document.getElementById('history-data').getElementsByTagName('tr');
  for(var i=0;i<rows.length;i++){
    if ( rows[i].textContent.indexOf(s)>0  || s.length==0 ) {
	  rows[i].style.display ='';
    } else {
      rows[i].style.display ='none';
    }
  }
}


var timer;
function delayedSearch() {
	clearTimeout(timer);
    timer = setTimeout(function () {
		search();
    }, 500);
  }

function historydel(id){
  $.ajax({
    url: "/history/del/" + id,
    success: function(){
      $("#h" + id).remove()
    },
    error: function(){
      console.log("Error al borrar el historial")

    }
  })
}

function historyupd(id){
  desc = $("#txt-description" + id).val()
  $.ajax({
    url: "/history/upddes/" + id,
    data: {desc: desc},
    type: "GET",
    success: function(){
    },
    error: function(xhr, status){
      console.log("Error al actualizar la description:"+status + " -h:" + xhr)
    }
  })
}

function edit_enter_desc(id){
  val = $("#td-description" + id).html().trim()
  $("#td-description" + id).html('<input type="text" id="txt-description' + id +'" class="form-control" placeholder="Query description" \
              onkeyup="historyupd(' + id +')" value="'+ val +'">')
  $("#txt-description" + id).focus()
  $("#txt-description" + id).focusout(function(){
    val_input = $("#txt-description" + id).val()
    $("#td-description" + id).html(val_input)
  })

}