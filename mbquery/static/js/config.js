$(document).ready(function(){
    console.log("Document ready de config")
    $("#configModal").on("show.bs.modal", function(event){
        console.log("Modal mostrado")
        render_config()

        $("#btn-save_config").click(function(){
            console.log("Boton de guardar")
            save_config()
        })

    })
})

function render_config(){
    console.log("En render config")
    $.ajax({
        url: "/config/get",
        dataType: "json",
        success: function(json){
            console.log("completado correcto")
            fill_config(json)
        },
        error: function(xhr, status){
            console.log("xht:", xhr)
        }
    })
}

function fill_config(json){
    $("#txt-project").val(json.config.project)
    $("#txt-delimiter").val(json.config.delimiter_txt)
    $("#txt-hist_dur_days").val(json.config.hist_dur_days)
    $("#col-source").html(json.html_source)
    $("#col-limit").html(json.html_limit)
    console.log("dark" + json.config.dark_mode)
    if (json.config.dark_mode == 1){
        console.log("activando dark")
        mod_options("#dark-active", "#dark-inactive")
        mod_label("#lbl-dark-active", "#lbl-dark-inactive")
    }else{
        console.log("desactivando dark")
        mod_options("#dark-inactive", "#dark-active")
        mod_label("#lbl-dark-inactive", "#lbl-dark-active")
    }

    if (json.config.vim_mode == 1){
        console.log("activando vim")
        mod_options("#vim-active", "#vim-inactive")
        mod_label("#lbl-vim-active", "#lbl-vim-inactive")
    }else{
        console.log("desactivando vim")
        mod_options("#vim-inactive", "#vim-active")
        mod_label("#lbl-vim-inactive", "#lbl-vim-active")
    }
}

function mod_options(id1, id2){
    $(id1).prop("checked", true)
    $(id2).prop("checked", false)
}

function mod_label(id1, id2){
    $(id1).addClass("active")
    $(id2).removeClass("active")
}

function save_config(){
    console.log("En save config")
    project = $("#txt-project").val()
    source = $("input[type=radio][name=config_opt_source]:checked").val()
    limit = $("#config_output-limit").val()
    delimiter = $("#txt-delimiter").val().substring(0, 1)
    hist_dur_days = $("#txt-hist_dur_days").val()
    dark_mode = $("input[type=radio][name=opt_darkmode]:checked").val()
    vim_mode = $("input[type=radio][name=opt_vimmode]:checked").val()
    console.log("datos recuperados")
    console.log("delim:" + delimiter)

    $.ajax({
        url: "/config/update",
        data: {project: project, source: source, limit: limit, delimiter: delimiter, hist_dur_days: hist_dur_days, dark_mode: dark_mode, vim_mode: vim_mode},
        type: "GET",
        beforeSend:function(){
            console.log("En before send de save config")
        },
        success: function(){
            console.log("Actualizado correctamente")
        },
        error(xhr, status){
            console.log("xhr:" + xhr)
        }
    })
}