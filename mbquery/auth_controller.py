import functools

from flask import (Blueprint, flash, g, redirect,
                   render_template, request, session, url_for)
from werkzeug.security import check_password_hash, generate_password_hash as gen_passwd
import click
from flask.cli import with_appcontext
from mbquery import constant
from mbquery.repository.user_repository import insert_user, get_user_by_username, get_user_by_id, update_last_login
from mbquery.repository.config_repository import insert_default_config

blueprint = Blueprint("auth", __name__, url_prefix="/auth")


@blueprint.route("/login", methods=("GET", "POST"))
def login():
    if request.method == "POST":
        username = request.form["username"]
        password = request.form["password"]

        error = None

        user = get_user_by_username(username)

        if user is None:
            error = constant.INCORRECT_USER
        elif not check_password_hash(user["password"], password):
            error = constant.INCORRECT_PASSWORD

        if error is None:
            session.clear()
            session["user_id"] = user["id"]
            update_last_login(username)

            return redirect(url_for("index"))

        flash(error)

    return render_template("auth/login.html")


@blueprint.route("/logout")
def logout():
    session.clear()
    return redirect(url_for("auth.login"))


def login_required(view):
    @functools.wraps(view)
    def wrapped_view(**kwargs):
        if g.user is None:
            return redirect(url_for("auth.login"))

        return view(**kwargs)

    return wrapped_view


@blueprint.before_app_request
def load_logged_in_user():
    user_id = session.get("user_id")

    if user_id is None:
        g.user = None
    else:
        g.user = get_user_by_id(user_id)


def add_user(username, password):
    """Agrega un nuevo usuario al sistema"""
    if not username:
        print(constant.USERNAME_MISSING)
        return
    elif not password:
        print(constant.PASSWORD_MISSING)
        return

    user = get_user_by_username(username)

    if user is not None:
        print(constant.USERNAME_ALREADY_EXIST)
        return
    insert_user(username, gen_passwd(password))
    user_id = get_user_by_username(username)["id"]
    insert_default_config(user_id)

@click.command("add-user")
@click.argument("username")
@click.argument("password")
@with_appcontext
def add_user_command(username, password):
    """Comando para agreagar un nuevo usuario"""
    add_user(username, password)
    click.echo(constant.CREATED_USER_OK)


def add_commands(app):
    app.cli.add_command(add_user_command)
