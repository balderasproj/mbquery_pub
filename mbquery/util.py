import json
from time import time
from flask import current_app as app
import os
from mbquery.reactive_html import get_html_pane_dropdown
from mbquery.constant import FORMATS


def get_string_from_list(data, duration, history_id):
    cols = data[0]
    rows = data[1:]

    result = [dict(zip(cols, row)) for row in rows]

    return json.dumps({"result": result, "duration": duration, "dropdown_export": get_html_pane_dropdown(history_id, FORMATS)})


def get_filename_result():
    filename = "F" + str(time()).replace(".","")
    path = os.path.join(app.root_path, app.config["RESULT_FILES"], filename)
    return filename, path

def dict_from_sqlite_row(row):
    return dict(zip(row.keys(), row))