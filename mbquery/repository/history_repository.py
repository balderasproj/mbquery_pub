from mbquery.db import get_db

def get_history():
    """Get list of queries from history table"""
    db = get_db()
    sql = """Select x.id, exec_date, query, 
    y.name source,
    z.name status,
    project, duration, user_id, coalesce(description, '') description
  From history x join
       source y
    on (x.source = y.id) join 
       status z
    on (x.status = z.id)
  order by exec_date desc"""
    return db.execute(sql).fetchall()


def save_history(query, source, project, username, status):
  """Save the row of history on master database"""
  db = get_db()
  sql = """
Insert Into history(query, source, project, user_id, status)
values (?, ?, ?, ?, ?)
  """
  db.execute(sql, (query, source, project, username, status))
  db.commit()
  print("history inserted")
  return _get_history_id(db)

def delete_history(id):
  """Delete history"""
  db = get_db()
  sql = "delete from history where id=?"
  db.execute(sql,(id,))
  db.commit()
  print("delete history:", id)

def update_history(id, status, duration, query_local, base_local):
  """Update history after run query"""    
  db = get_db()
  sql = """
  Update history 
     set status=?,
         duration=?,
         query_local=?,
         base_local=?
  Where id=?
  """
  db.execute(sql,(status, duration, query_local, base_local, id))
  db.commit()
  print("history updated")

def update_history_des(id, description):
  """Update history after run query"""    
  db = get_db()
  sql = """
  Update history 
     set description=?
  Where id=?
  """
  print("SQL:", sql)
  print("id", id)
  print("desc", description)
  db.execute(sql,(description, id))
  db.commit()
  print("history description updated")

def _get_history_id(db):
      sql = "Select max(id) max_id from history"
      return db.execute(sql).fetchall()[0][0]

def get_history_by_id(id):
      sql = " Select query, 'select * from ' || query_local query_local, base_local from history where id=?"
      return get_db().execute(sql,(id,)).fetchall()
    