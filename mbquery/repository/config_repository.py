from mbquery.db import get_db
from mbquery import constant

def insert_default_config(user_id):
    db = get_db()
    sql = """
    Insert Into config(project, limit_rows, source, hist_dur_days, delimiter_txt, vim_mode, dark_mode, user_id)
    values(?, ?, ?, ?, ?, ?, ?, ?)
    """
    db.execute(sql,("", constant.LIMIT_5, constant.SOURCE_TD, 1000, "~", 1, 0, user_id ))
    db.commit()
    print("config inserted for user:", user_id)


def update_config(user_id, project, limit, source, hist_dur_days, delimiter, vim_mode, dark_mode):
    db = get_db()
    sql = """
    Update config
       set project = ?,
           limit_rows = ?,
           source = ?,
           hist_dur_days = ?,
           delimiter_txt = ?,
           vim_mode = ?,
           dark_mode = ?
     Where user_id = ?
    """
    db.execute(sql, (project, limit, source, hist_dur_days, delimiter, vim_mode, dark_mode, user_id))
    db.commit()
    print("config updated for user:", user_id)


def get_config(user_id):
    sql = """
    Select project, limit_rows, source, hist_dur_days, delimiter_txt, vim_mode, dark_mode
      From config
    Where user_id = ?
    """

    return get_db().execute(sql,(user_id,)).fetchone()


def _get_config_field(user_id, field):
    sql = f"""
    Select {field}
      From config
    Where user_id = ?
    """
    result = get_db().execute(sql,(user_id,)).fetchone()
    return result[f"{field}"]


def get_config_project(user_id):
    return _get_config_field(user_id, "project")


def get_config_limit(user_id):
    return _get_config_field(user_id, "limit_rows")
    

def get_config_source(user_id):
    return _get_config_field(user_id, "source")


def get_config_delimiter(user_id):
    return _get_config_field(user_id, "delimiter_txt")