from mbquery.db import get_db

def insert_user(username, password):
    db = get_db()
    sql = "insert into user(username, password) values (?, ?)"
    db.execute(sql, (username, password))
    db.commit()


def get_user_by_username(username):
    db = get_db()
    sql = "Select * from user where username = ?"
    return db.execute(sql, (username,)).fetchone()


def get_user_by_id(user_id):
    db = get_db()
    sql = "Select * from user where id = ?"
    return db.execute(sql, (user_id,)).fetchone()

def update_last_login(username):
    db = get_db()
    sql = "Update user set last_login=current_timestamp where username=?"
    db.execute(sql,(username,))
    db.commit()
    print("Login update")
