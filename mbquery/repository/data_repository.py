from mbquery.database import Database
from flask import current_app as app
from datetime import datetime
from time import time
import os


def store_local(data):
    database, tablename = _get_local_database()
    db = Database(os.path.join(app.instance_path, database))
    db.load_into_table(tablename, data)
    return  database, tablename


def _get_local_database():
    database = "b" + datetime.now().strftime("%H%M%S_%f") + ".db"
    tablename = "t" + str(time()).replace(".","")
    return database, tablename


