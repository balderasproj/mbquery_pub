from flask import (Blueprint, flash, g, redirect,
                   render_template, request, session, url_for, current_app)
from werkzeug.exceptions import abort
from mbquery.auth_controller import login_required
from mbquery.repository.config_repository import get_config, update_config
from mbquery.util import dict_from_sqlite_row
from mbquery import constant
from mbquery.reactive_html import get_html_config_source, get_html_config_limit


blueprint = Blueprint('config', __name__, url_prefix="/config")


@blueprint.route("/get")
@login_required
def get():
    """Return de config data"""
    config = dict_from_sqlite_row(get_config(g.user["id"]))
    data = {"html_source": get_html_config_source(config.get("source"), constant.SOURCES_LIST),
        "html_limit": get_html_config_limit(config.get("limit_rows"), constant.LIMITS_LIST),
        "config": config}
    return data


@blueprint.route("/update", methods=("GET", "POST"))
@login_required
def update():
    if request.method == "GET":
        project = request.args.get("project")
        limit = request.args.get("limit")
        source = request.args.get("source")
        hist_dur_days = request.args.get("hist_dur_days")
        delimiter = request.args.get("delimiter")
        delimiter = delimiter[:1]
        vim_mode = request.args.get("vim_mode")
        dark_mode = request.args.get("dark_mode")

        update_config(g.user["id"], project, limit, source, hist_dur_days, delimiter, vim_mode, dark_mode)

    return redirect(url_for("sqlws.index"))




        

        