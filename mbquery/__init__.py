import os
from flask import Flask
from . import db
from . import auth_controller
from . import sqlws_controller
from . import history_controller
from . import error_controller
from . import config_controller


def create_app(test_config=None):
    """ Crear y configurar app """

    app = Flask(__name__, instance_relative_config=True)
    app.config.from_mapping(
        SECRET_KEY="dev",
        DATABASE=os.path.join(app.instance_path, "mbquery.db"))

    if test_config is None:
        app.config.from_pyfile("config.py", silent=True)
    else:
        app.config.from_mapping(test_config)

    app.config["RESULT_FILES"] = "resfiles"

    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    db.init_app(app)
    app.register_blueprint(auth_controller.blueprint)
    app.register_blueprint(sqlws_controller.blueprint)
    app.register_blueprint(history_controller.blueprint)
    app.register_blueprint(config_controller.blueprint)
    app.register_error_handler(404, error_controller.page_not_found)
    app.add_url_rule("/", endpoint="index")
    app.add_url_rule("/history", endpoint="history")
    auth_controller.add_commands(app)

    @app.route("/test_server")
    def test_server():
        return "Todo esta funcionando correctamente :D"

    return app
