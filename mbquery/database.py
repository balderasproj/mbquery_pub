import sqlite3
import sys

class Database:

    __slots__ = ["conn", "curs", "_status"]

    def __init__(self, database):
        self.conn = sqlite3.connect(database)
        self.curs = self.conn.cursor()
        self._status = 0

    def execute_and_fechtall(self, sql):
        try:
            data = self.curs.execute(sql).fetchall()
            INDEX_COLNAME = 0
            cols = [info[INDEX_COLNAME] for info in self.curs.description]
            self._status = 1
            return [cols,] + data
        except sqlite3.Error as ERR:
            self._status = 0
            return [["Error"],["Syntax error in the SQL: " + ERR.args[0]]]
    
    def get_status(self):
        return self._status

    def _get_cols(self, tablename):
        INDEX_COLNAME = 1
        sql = f"pragma table_info('{tablename}')"
        result = self.curs.execute(sql).fetchall() 

        cols = []
        if result is None:
            return cols
        cols = [row[INDEX_COLNAME] for row in result]
        return cols

    def load_into_table(self, table, data):
        if not data:
            return

        self.execute_sql(self._get_ddl(table, data[0]))
        self._load(table, data)
        
    def _load(self, table, data, truncate=True, first_is_header=True):
        if truncate:
            self.execute_sql(f"delete from {table}")

        rows = data
        if first_is_header:
            rows = data[1:]

        if rows:
            self.curs.executemany(self._get_string_insert(table, len(rows[0])), rows)
            self.conn.commit()

    def execute_sql(self, sql):
        try:
            self.curs.execute(sql)
        except sqlite3.OperationalError:
            print("Error al ejecutar el sql:", sys.exc_info()[0])

    def attach_db(self, filename, dbname):
        try:
            sql = f"attach database ? as ?"
            self.curs.execute(sql, (filename, dbname))
            print("Database attached:", dbname)
        except sqlite3.OperationalError:
            print("Error al ejecutar el sql:", sys.exc_info()[0])
       
    def _get_string_insert(self, table, cols_len):
        sql = f"insert into {table} values ("
        for n in range(0,cols_len):
            sql += "?,"
        sql = sql[0:len(sql)-1] + ")"
        return sql
    
    def _get_ddl(self, table, cols):
        ddl = f"create table {table} ("
        for col in cols:
            ddl += col.replace(" ", "_").replace("*","all").replace("(","_").replace(")","_").replace("notnull","notnull_") + " text,"

        ddl = ddl[0:len(ddl)-1] + ")"
        return ddl



